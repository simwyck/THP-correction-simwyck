Rails.application.routes.draw do

  post 'film/search', to:'films#search', as: 'search'

  root 'films#index'

end
